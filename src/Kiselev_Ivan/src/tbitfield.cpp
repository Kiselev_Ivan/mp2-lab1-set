#include "tbitfield.h"
#include <bitset>
#include "math.h"

const int bByte = 8;		//количество битов в байте

TBitField::TBitField(int len)
{
	BitLen = len;

	if (len < 0)
		throw 1;
	if ((len % (sizeof(TELEM) * bByte) == 0))
		MemLen = len / (sizeof(TELEM) * bByte);
	else
		MemLen = len / (sizeof(TELEM) * bByte) + 1;

	pMem = new TELEM[MemLen];

	if (pMem == NULL)
		throw 2;

	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;

	pMem = new TELEM[MemLen];

	if (pMem == NULL)
		throw 2;

	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	delete pMem;
}

int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if ((n + 1) % (sizeof(TELEM) * bByte) == 0)
		return (n + 1) / (sizeof(TELEM) * bByte) - 1;
	else
		return (n + 1) / (sizeof(TELEM) * bByte);
}

TELEM TBitField::GetMemMask(const int n)  const // битовая маска для бита n		
{
	TELEM temp = 1;
	int shift = n % (sizeof(TELEM) * bByte);
	temp = temp << shift;
	return temp;
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
	return (*this).BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n >= 0 && n < BitLen)
		this->pMem[GetMemIndex(n)] = this->pMem[GetMemIndex(n)] | GetMemMask(n);
	else
		throw 3;
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if ((n < 0) || (n >= GetLength()))
	{
		throw 3;
	};
	int i;
	i = GetMemIndex(n);
	pMem[i] &= (~GetMemMask(n));
}

int TBitField::GetBit(const int n) const // получить значение бита	
{
	if (n >= 0 && n < BitLen)
		return ((pMem[GetMemIndex(n)] & GetMemMask(n)) ? 1 : 0);
	else
		throw 3;

}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (this == &bf)
		return *this;
	else
	{
		BitLen = bf.BitLen;
		MemLen = bf.MemLen;

		delete[] pMem;

		pMem = new TELEM[MemLen];

		if (pMem == NULL)
			throw 2;

		for (int i = 0; i < MemLen; i++)
			pMem[i] = bf.pMem[i];

		return *this;
	}
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	int c = 1;
	if (BitLen != bf.BitLen)
	{
		c = 0;
	}
	else
	{
		for (int i = 0; i<BitLen; i++)
			if ((this->GetBit(i)) != (bf.GetBit(i)))
			{
				c = 0;
				break;
			};
	};
	return c;

}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	if ((*this) == bf) { return 0; }
	else { return 1; };
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField res((BitLen > bf.BitLen) ? BitLen : bf.BitLen);
	for (int i = 0; i < MemLen; i++)
		res.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		res.pMem[i] = res.pMem[i] | bf.pMem[i];
	return res;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"	
{
	int i, len = BitLen;
	if (bf.BitLen > len) len = bf.BitLen;
	TBitField temp(len);
	for (i = 0; i < MemLen; i++) temp.pMem[i] = pMem[i];
	for (i = 0; i < bf.MemLen; i++) temp.pMem[i] &= bf.pMem[i];
	return temp;
}

TBitField TBitField::operator~(void) // отрицание
{
	int len = BitLen;
	TBitField res(len);
	for (int i = 0; i < MemLen; i++)
		res.pMem[i] = ~(pMem[i]);
	return res;
}

// ввод/вывод

istream &operator >> (istream &istr, TBitField &bf) // ввод
{
	unsigned int c;
	int i = 0;
	for (int j = 0; j < sizeof(TELEM); j++)
	{
		istr >> c;
		if (c == 1) { bf.SetBit(j); };
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.BitLen; i++)
		ostr << bf.GetBit(i);

	return ostr;
}
