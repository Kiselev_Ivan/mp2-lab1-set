#include "tbitfield.h"
#include "tset.h"

TSet::TSet(const int mp) : BitField(mp)
{
	MaxPower = mp;
}

// конструктор копирования
TSet::TSet(const TSet &s) : BitField(s.BitField)
{
	MaxPower = s.MaxPower;
}

// конструктор преобразования типа
TSet::TSet(const TBitField &bf) : BitField(bf)
{
	MaxPower = bf.GetLength();
}

TSet::operator TBitField()		// преобразование типа к битовому полю		
{
	TBitField temp(this->BitField);
	return temp;
}

int TSet::GetMaxPower(void) const // получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // элемент множества?
{
	if (this->BitField.GetBit(Elem) == 1) { return 1; }
	else { return 0; };
}

void TSet::InsElem(const int Elem) // включение элемента множества
{
	this->BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // исключение элемента множества
{
	this->BitField.ClrBit(Elem);
}

// теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // присваивание  
{
	MaxPower = s.MaxPower;
	BitField = s.BitField;
	return (*this);
}

int TSet::operator==(const TSet &s) const // сравнение
{
	if ((this->GetMaxPower() == s.GetMaxPower()) && (this->BitField == s.BitField))
	{
		return 1;
	}
	else { return 0; };
}

int TSet::operator!=(const TSet &s) const // сравнение
{
	if ((*this) == s) {
		return 0;
	}
	else { return 1; };
}

TSet TSet::operator+(const TSet &s) // объединение
{
	int max;
	if (this->GetMaxPower() >= s.GetMaxPower())
	{
		max = this->GetMaxPower();
	}
	else
	{
		max = s.GetMaxPower();
	};
	TSet res(max);
	res.BitField = this->BitField | s.BitField;
	return res;
}

TSet TSet::operator+(const int Elem) // объединение с элементом
{
	this->BitField.SetBit(Elem);
	return (*this);
}

TSet TSet::operator-(const int Elem) // разность с элементом
{
	this->BitField.ClrBit(Elem);
	return (*this);
}

TSet TSet::operator*(const TSet &s) // пересечение
{
	int max;
	if (this->GetMaxPower() >= s.GetMaxPower())
	{
		max = this->GetMaxPower();
	}
	else
	{
		max = s.GetMaxPower();
	};
	TSet res(max);
	res.BitField = this->BitField & s.BitField;
	return res;
}

TSet TSet::operator~(void) // дополнение
{
	return ~(this->BitField);
}

// перегрузка ввода/вывода

istream &operator >> (istream &istr, TSet &s) // ввод
{
	int c;
	istr >> c;
	s.BitField.SetBit(c);
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // вывод
{
	ostr << "{ ";
	for (int i = 0; i < s.GetMaxPower(); i++)
		if (s.BitField.GetBit(i) == 1) { ostr << i << ' '; };
	ostr << '}';
	return ostr;
}
